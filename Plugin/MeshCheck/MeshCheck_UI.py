import bpy

class MeshCheckPanel:
    """Default panel type."""
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "MeshCheck"
    bl_options = {"DEFAULT_CLOSED"}


class FIX_PT_MeshCheck_Transforms(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "RENDER_PT_MeshCheck_Transforms"
    bl_label = "Apply Transforms"

    @classmethod
    def poll(cls, context):
        return bpy.context.mode == "OBJECT"

    def draw(self, context):
        layout = self.layout
        # layout.label(text="Transforms check.")

        scene = context.scene
        myTool = scene.my_tool

        layout.prop(myTool, "applyTransformLocation",   text="Apply Location")
        layout.prop(myTool, "applyTransformRotation",   text="Apply Rotation")
        layout.prop(myTool, "applyTransformScale",      text="Apply Scale")

        layout.operator("object.apply_transforms", text="Apply Transforms")


class RENDER_PT_MeshCheck_Shading(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "RENDER_PT_MeshCheck_Shading"
    bl_label = "Normal Visualisation"

    @classmethod
    def poll(cls, context):
        return bpy.context.mode == "OBJECT" or bpy.context.mode == "EDIT_MESH" 

    def draw(self, context):
        layout = self.layout

        # Face Orientation.
        row = layout.row()
        row.prop(context.space_data.overlay, "show_face_orientation", text="Normal Orientation")

        # Normal Lines In Edit Mode
        if bpy.context.mode == "EDIT_MESH":
            row = layout.row()
            row.prop(context.space_data.overlay, "show_vertex_normals", text="Vertex Normals")
            row = layout.row()
            row.prop(context.space_data.overlay, "show_face_normals", text="Face Normals")
            row = layout.row()
            row.prop(context.space_data.overlay, "show_split_normals", text="Split Normals")
            row = layout.row()
            row.prop(context.space_data.overlay, "normals_length")

        # Back Face Culling
        row = layout.row()
        row.prop(bpy.context.space_data.shading, "show_backface_culling")


class FIX_PT_MeshCheck_Shading(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "FIX_PT_MeshCheck_Shading"
    bl_label = "Fix Normals"

    @classmethod
    def poll(cls, context):
        return bpy.context.object is not None

    def draw(self, context):
        layout = self.layout
        # layout.label(text="Normal Fix.")

        if bpy.context.mode == "OBJECT":
            row = layout.row()
            row.operator("object.shade_flat")
            row.operator("object.shade_smooth")

        if bpy.context.mode == "EDIT_MESH":
            row = layout.row()
            row.operator("mesh.faces_shade_flat")
            row.operator("mesh.faces_shade_smooth")
            

        row = layout.row()
        row.prop(bpy.context.object.data, "use_auto_smooth")
        row.prop(bpy.context.object.data, "auto_smooth_angle")
        row = layout.row()


        if bpy.context.mode == "OBJECT":
            row = layout.row()
            row.operator("object.remove_custom_split_normals")
            row = layout.row()  
            row.operator("object.add_weighted_normal_modifier")
        
        if bpy.context.mode == "EDIT_MESH":
            row = layout.row()
            row.operator("mesh.flip_normals")
            row = layout.row()
            row.operator("mesh.normals_recalculate_outside")
            row = layout.row()
            row.operator("mesh.normals_recalculate_inside")


class FIND_PT_MeshCheck_Geometry(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "FIND_PT_MeshCheck_Geometry"
    bl_label = "Bad Geometry Selection"

    @classmethod
    def poll(cls, context):
        return context.mode == "EDIT_MESH"

    def draw(self, context):
        layout = self.layout
        # layout.label(text="Geometry Find")

        row = layout.row()
        row.operator("mesh.select_linked_parts")

        row = layout.row()
        row = layout.row()
        row.operator("mesh.select_tris")
        row = layout.row()
        row.operator("mesh.select_ngons")

        row = layout.row()
        row = layout.row()
        row.operator("mesh.select_non_manifold")
        row = layout.row()
        row.operator("mesh.select_interior_faces")
        row = layout.row()
        row.operator("mesh.select_holes")
        row = layout.row()
        row.operator("mesh.select_complex_elements")


class FIX_PT_MeshCheck_Geometry(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "FIX_PT_MeshCheck_Geometry"
    bl_label = "Geometry Fix"

    def draw(self, context):
        layout = self.layout
        # layout.label(text="Geometry stuff")

        if context.mode == "OBJECT":
            row = layout.row()
            row.operator("object.add_decimate_modifier")
            row = layout.row()
            row.operator("object.add_remesh_modifier")
            row = layout.row()
            row.operator("object.add_triangulate_modifier")
            row = layout.row()
            row.operator("object.add_weld_modifier")

        if context.mode == "EDIT_MESH":
            row = layout.row()
            row.operator("mesh.remove_doubles")

            row = layout.row()            
            row = layout.row()
            row.operator("mesh.fill_grid")            
            row = layout.row()
            row.operator("mesh.f2", text="Ngon Fill")            
            row = layout.row()
            row.operator("mesh.fill", text="Triangle Fill")            

            row = layout.row()
            row = layout.row()
            row.operator("mesh.separate_selected")
            row = layout.row()
            row.operator("mesh.separate_by_loose_parts")


class RENDER_PT_MeshCheck_Modifiers(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "RENDER_PT_MeshCheck_Modifiers"
    bl_label = "Modifiers utils"

    @classmethod
    def poll(cls, context):
        return bpy.context.mode == "OBJECT"

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator("object.add_fix_modifiers")
        row = layout.row()
        row.operator("object.apply_all_modifiers")


class FIX_PT_MeshCheck_VMCK_FIX(MeshCheckPanel, bpy.types.Panel):
    bl_idname = "RENDER_PT_MeshCheck_VMCK_FIX"
    bl_label = "VMCK Fix"

    @classmethod
    def poll(cls, context):
        return bpy.context.mode == "OBJECT"

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator("object.vmck_fix")


classes = (
    FIX_PT_MeshCheck_Transforms,
    RENDER_PT_MeshCheck_Shading,
    FIX_PT_MeshCheck_Shading,
    FIND_PT_MeshCheck_Geometry,
    FIX_PT_MeshCheck_Geometry,
    RENDER_PT_MeshCheck_Modifiers,
    FIX_PT_MeshCheck_VMCK_FIX,
)


def register():
    for _class in classes:
        bpy.utils.register_class(_class)

def unregister():
    for _class in classes:
        bpy.utils.unregister_class(_class)
