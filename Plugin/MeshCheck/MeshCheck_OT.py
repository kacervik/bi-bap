import bpy

class EXECUTE_OT_MeshCheck_ApplyTransform(bpy.types.Operator):
    bl_idname = "object.apply_transforms"
    bl_label = "Apply Transforms"
    bl_description = "Apply Transforms on Active Object"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None and bpy.context.mode == "OBJECT"

    def execute(self, context):
        scene = context.scene
        myTool = scene.my_tool

        bpy.ops.object.transform_apply(location=myTool.applyTransformLocation, rotation=myTool.applyTransformRotation, scale=myTool.applyTransformScale)

        return {'FINISHED'}


class  EXECUTE_OT_MeshCheck_Apply_Flat_Shading(bpy.types.Operator):
    bl_idname = "object.flat_shading"
    bl_label = "Flat Shading"
    bl_description = "Applies flat shading"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        bpy.ops.object.shade_flat()

        return {'FINISHED'}


class  EXECUTE_OT_MeshCheck_Apply_Smooth_Shading(bpy.types.Operator):
    bl_idname = "object.smooth_shading"
    bl_label = "Smooth Shading"
    bl_description = "Applies smooth shading"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        bpy.ops.object.shade_smooth()

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Remove_Custom_Split_Normals(bpy.types.Operator):
    bl_idname = "object.remove_custom_split_normals"
    bl_label = "Remove Custom Normals"
    bl_description = "Clears custom split normals"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        bpy.ops.mesh.customdata_custom_splitnormals_clear()

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Add_Weighted_Normal_Modifier(bpy.types.Operator):
    bl_idname = "object.add_weighted_normal_modifier"
    bl_label = "Add Weighted Normal Modifier"
    bl_description = "Adds weighted normal modifier"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        bpy.ops.object.modifier_add(type='WEIGHTED_NORMAL')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Normals_Recalculate_Outside(bpy.types.Operator):
    bl_idname = "mesh.normals_recalculate_outside"
    bl_label = "Recalculate Outside"
    bl_description = "Points normals outisde"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.normals_make_consistent(inside=False)
        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Normals_Recalculate_Inside(bpy.types.Operator):
    bl_idname = "mesh.normals_recalculate_inside"
    bl_label = "Recalculate Inside"
    bl_description = "Points normals inside"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.normals_make_consistent(inside=True)
        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Add_Decimate_Modifier(bpy.types.Operator):
    bl_idname = "object.add_decimate_modifier"
    bl_label = "Add Decimate Modifier"
    bl_description = "Adds decimate modifier"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "OBJECT"

    def execute(self, context):
        bpy.ops.object.modifier_add(type='DECIMATE')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Add_Remesh_Modifier(bpy.types.Operator):
    bl_idname = "object.add_remesh_modifier"
    bl_label = "Add Remesh Modifier"
    bl_description = "Adds remesh modifier"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "OBJECT"

    def execute(self, context):
        bpy.ops.object.modifier_add(type='REMESH')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Add_Triangulate_Modifier(bpy.types.Operator):
    bl_idname = "object.add_triangulate_modifier"
    bl_label = "Add Triangulate Modifier"
    bl_description = "Adds triangulate modifier"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "OBJECT"

    def execute(self, context):
        bpy.ops.object.modifier_add(type='TRIANGULATE')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Add_Weld_Modifier(bpy.types.Operator):
    bl_idname = "object.add_weld_modifier"
    bl_label = "Add Weld Modifier"
    bl_description = "Adds weld modifier"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "OBJECT"

    def execute(self, context):
        bpy.ops.object.modifier_add(type='WELD')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Separate_By_Loose_Parts(bpy.types.Operator):
    bl_idname = "mesh.separate_by_loose_parts"
    bl_label = "Separate by Loose Parts"
    bl_description = "Separates mesh by its loose parts"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.separate(type='LOOSE')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Separate_Selected(bpy.types.Operator):
    bl_idname = "mesh.separate_selected"
    bl_label = "Separate Selected"
    bl_description = "Separates Selected Elements"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.separate(type='SELECTED')

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Select_Linked_Parts(bpy.types.Operator):
    bl_idname = "mesh.select_linked_parts"
    bl_label = "Select Linked"
    bl_description = "Selects linked elements"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.select_linked(delimit=set())

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Select_Tris(bpy.types.Operator):
    bl_idname = "mesh.select_tris"
    bl_label = "Select Triangles"
    bl_description = "Selects all triangles in the mesh"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.select_face_by_sides(number = 3, type='EQUAL', extend = False)

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Select_Ngons(bpy.types.Operator):
    bl_idname = "mesh.select_ngons"
    bl_label = "Select Ngons"
    bl_description = "Selects all ngnons in the mesh"

    @classmethod
    def poll(cls, context):
        return context.object is not None and bpy.context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.select_face_by_sides(number = 4, type='GREATER', extend = False)

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Select_Holes(bpy.types.Operator):
    bl_idname = "mesh.select_holes"
    bl_label = "Select Holes"
    bl_description = "Selects holes and boundaries"

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='EDGE')
        bpy.ops.mesh.select_non_manifold(use_boundary=True)

        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_Select_Complex_Elements(bpy.types.Operator):
    bl_idname = "mesh.select_complex_elements"
    bl_label = "Select Complex Elements"
    bl_description = "Selects complex elements and singularities"

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.mode == "EDIT_MESH"

    def execute(self, context):
        bpy.ops.mesh.select_non_manifold(extend=False, use_wire=False, use_boundary=False, use_multi_face=True, use_non_contiguous=False, use_verts=True)

        return {'FINISHED'}


def applyAllModifiers(context):
    activeObject = bpy.context.active_object

    for _modifier in activeObject.modifiers:
        bpy.ops.object.modifier_apply(modifier=_modifier.name)


class EXECUTE_OT_MeshCheck_Apply_All_Modifiers(bpy.types.Operator):
    bl_idname = "object.apply_all_modifiers"
    bl_label = "Apply All"
    bl_description = "Apply All Modifiers on Active Object"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        applyAllModifiers(context)
        return {'FINISHED'}


def addFixModifiers(context):
    activeObject = bpy.context.active_object

    activeObject.modifiers.new(name="Weld", type="WELD")

    decimateMod = activeObject.modifiers.new(name="Decimate", type="DECIMATE")
    decimateMod.decimate_type = 'DISSOLVE'
    decimateMod.angle_limit = 0.0174533

    triangulateMod = activeObject.modifiers.new(name="Triangulate", type="TRIANGULATE")
    triangulateMod.quad_method = 'BEAUTY'
    triangulateMod.ngon_method = 'BEAUTY'


class EXECUTE_OT_MeshCheck_Apply_Fix_Modifiers(bpy.types.Operator):
    bl_idname = "object.add_fix_modifiers"
    bl_label = "Modifier Fix"
    bl_description = "Applies several modifiers in order to fix mesh issues"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        addFixModifiers(context)
        return {'FINISHED'}


class EXECUTE_OT_MeshCheck_VMCK_FIX(bpy.types.Operator):
    bl_idname = "object.vmck_fix"
    bl_label = "VMCK Fix"
    bl_description = "Applies destructive fixing operation on VMCK buildings"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        addFixModifiers(context)

        bpy.ops.mesh.customdata_custom_splitnormals_clear()
        bpy.ops.object.shade_smooth()
        bpy.context.object.data.use_auto_smooth = True
        bpy.context.object.data.auto_smooth_angle = 0.174533

        applyAllModifiers(context)

        return {'FINISHED'}


classes = (
    EXECUTE_OT_MeshCheck_ApplyTransform,
    # EXECUTE_OT_MeshCheck_Apply_Flat_Shading,
    # EXECUTE_OT_MeshCheck_Apply_Smooth_Shading,
    EXECUTE_OT_MeshCheck_Remove_Custom_Split_Normals,
    EXECUTE_OT_MeshCheck_Add_Weighted_Normal_Modifier,
    EXECUTE_OT_MeshCheck_Normals_Recalculate_Outside,
    EXECUTE_OT_MeshCheck_Normals_Recalculate_Inside,
    EXECUTE_OT_MeshCheck_Add_Decimate_Modifier,
    EXECUTE_OT_MeshCheck_Add_Remesh_Modifier,
    EXECUTE_OT_MeshCheck_Add_Triangulate_Modifier,
    EXECUTE_OT_MeshCheck_Add_Weld_Modifier,
    EXECUTE_OT_MeshCheck_Separate_By_Loose_Parts,
    EXECUTE_OT_MeshCheck_Separate_Selected,
    EXECUTE_OT_MeshCheck_Select_Linked_Parts,
    EXECUTE_OT_MeshCheck_Select_Tris,
    EXECUTE_OT_MeshCheck_Select_Ngons,
    EXECUTE_OT_MeshCheck_Select_Holes,
    EXECUTE_OT_MeshCheck_Select_Complex_Elements,
    EXECUTE_OT_MeshCheck_Apply_All_Modifiers,
    EXECUTE_OT_MeshCheck_Apply_Fix_Modifiers,
    EXECUTE_OT_MeshCheck_VMCK_FIX,
)

def register():
    for _class in classes:
        bpy.utils.register_class(_class)

def unregister():
    for _class in classes:
        bpy.utils.unregister_class(_class)
