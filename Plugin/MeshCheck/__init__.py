# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "MeshCheck",
    "author" : "Viktor Kacer",
    "description" : "Addon for my bachelor thesis. Locates and fixes most common object and mesh problems.",
    "blender" : (2, 91, 0),
    "version" : (1, 0, 0),
    "location" : "Side Panel (N)",
    "warning" : "",
    "category" : "Generic"
}

from . import ( 
    MeshCheck_PT,
    MeshCheck_UI,
    MeshCheck_OT
)

def register():
    MeshCheck_PT.register()
    MeshCheck_UI.register()
    MeshCheck_OT.register()

def unregister():
    MeshCheck_PT.unregister()
    MeshCheck_UI.unregister()
    MeshCheck_OT.unregister()
