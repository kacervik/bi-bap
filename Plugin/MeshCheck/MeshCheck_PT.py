import bpy

from bpy.props import (
    BoolProperty,
    FloatProperty,
    PointerProperty,
)

from bpy.types import (
    PropertyGroup,
)

class myProperties(PropertyGroup):

    # --- Transforms ---
    applyTransformLocation : BoolProperty (
        name = "applyTransformLocation",
        description = "Apply location transform",
        default = False
    )

    applyTransformRotation : BoolProperty (
        name = "applyTransformRotation",
        description = "Apply rotation transform",
        default = False
    )

    applyTransformScale : BoolProperty (
        name = "applyTransformScale",
        description = "Apply scale transform",
        default = False
    )
    # ==================

    # --- Normals ---
    # normalVertex : BoolProperty (
    #     name = "normalVertex",
    #     description = "Show vertex normal orientation",
    #     default = False
    # )

    # normalFace : BoolProperty (
    #     name = "normalFace",
    #     description = "Show face normal orientation",
    #     default = False
    # )

    # normalSplitVertex : BoolProperty (
    #     name = "normalSplitVertex",
    #     description = "Show split-vertex normal orientation",
    #     default = False
    # )

    # normalLineLenght : FloatProperty (
    #     name = "normalLineLenght",
    #     description = "Sets line length of normals",
    #     default = 0.5,
    #     min = 0,
    #     soft_max = 2,
    # )
    # ===============


def register():
    bpy.utils.register_class(myProperties)
    bpy.types.Scene.my_tool = PointerProperty(type=myProperties)

def unregister():    
    bpy.utils.unregister_class(myProperties)
    del bpy.types.Scene.my_tool
